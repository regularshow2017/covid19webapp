import React, { Component } from "react";
import { Route, Routes } from "react-router-dom";
import Navbar from "./components/navbar/Navbar.js";
import Home from "./routes/Home.js";
import About from "./routes/aboutApp.js";
import Info from "./routes/aboutCovid.js";
import "./App.css";

/**
 * Class to create App component
 * */
class App extends Component {
  /**
   * @returns {ReactElement} App component
   */
  render() {
    return (
      <React.Fragment>
        <Navbar />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="aboutApp" element={<About />} />
          <Route exact path="aboutCovid" element={<Info />}></Route>
        </Routes>
      </React.Fragment>
    );
  }
}

export default App;
