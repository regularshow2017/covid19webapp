import "./Sidebar.css";

/**
 * Sidebar component
 * @module Sidebar component
 * @param {Object} isOpen - Sidebar open state
 * @returns {ReactElement} Sidebar component
 */
export default function Sidebar({ isOpen }) {
  return (
    <>
      <div data-testid="sidebar-test" className="sidebar">
        <div className={isOpen ? "bar bar1--open" : "bar bar1"} />
        <div className={isOpen ? "bar bar2--open" : "bar bar2"} />
        <div className={isOpen ? "bar bar3--open" : "bar bar3"} />
      </div>
    </>
  );
}
