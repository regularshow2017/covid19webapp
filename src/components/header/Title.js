import { useState } from "react";
import { MdCoronavirus } from "react-icons/md";
import { Outlet, Link } from "react-router-dom";
import Sidebar from "./Sidebar";

/**
 * Title component
 * @module Title component
 * @returns {ReactElement}
 */

const Title = () => {
  const [sideBar, setSideBar] = useState(false);
  const toggleSideBar = () => {
    setSideBar(!sideBar);
  };
  return (
    <div data-testid="title-test" className="nav">
      <Link className="nav--title noline" to="/">
        <MdCoronavirus size="55px" className="covidIcon" />
        <span className="title">COVID-19 US Information</span>
      </Link>
      <div className={sideBar ? "nav--link" : "no--display"}>
        <ul>
          <Link className="noline" to="/aboutApp">
            <li>About the App</li>
          </Link>
          <Link className="noline" to="/aboutCovid">
            <li>What is Covid-19?</li>
          </Link>
          <Outlet />
        </ul>
      </div>
      <div className="sidebar" onClick={toggleSideBar}>
        <Sidebar isOpen={sideBar} />
      </div>
    </div>
  );
};

export default Title;
