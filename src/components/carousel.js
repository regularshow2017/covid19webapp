import React, { useEffect, useState } from "react";
import { useSwipeable } from "react-swipeable";
import "./carousel.css";

export const CarouselItem = ({ children, width } = {}) => {
  return (
    <div
      data-testid="carousel-item-test"
      className="carousel-item"
      style={{ width: width }}
    >
      {children}
    </div>
  );
};

/**
 * Carousel component
 * @param {Object} props - Carousel component properties
 * @returns {ReactElement}
 */
const Carousel = ({ children }) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [paused, setPaused] = useState(false);

  const updateIndex = (newIndex) => {
    if (newIndex < 0) {
      newIndex = 0;
    } else if (newIndex >= React.Children.count(children)) {
      newIndex = React.Children.count(children) - 1;
    }
    setActiveIndex(newIndex);
  };

  useEffect(() => {
    const interval = setInterval(() => {
      if (!paused) {
        updateIndex(activeIndex + 1);
        if (activeIndex === React.Children.count(children) - 1) {
          updateIndex(0);
        }
      }
    }, 7000);
    return () => {
      if (interval) {
        clearInterval(interval);
      }
    };
  });

  const swipeHandlers = useSwipeable({
    onSwipedLeft: () => updateIndex(activeIndex + 1),
    onSwipedRight: () => updateIndex(activeIndex - 1),
  });

  return (
    <div
      {...swipeHandlers}
      className="carousel"
      onMouseEnter={() => setPaused(true)}
      onMouseLeave={() => setPaused(true)}
      data-testid="carousel-test"
    >
      <div
        className="inner"
        style={{ transform: `translateX(-${activeIndex * 100}%)` }}
      >
        {React.Children.map(children, (child) => {
          return React.cloneElement(child, { width: "100%" });
        })}
      </div>
      <div className="indicators">
        <button
          className={`indicator ${activeIndex === 0 ? "active" : ""}`}
          onClick={() => updateIndex(0)}
        ></button>
        <button
          className={`indicator ${activeIndex === 1 ? "active" : ""}`}
          onClick={() => updateIndex(1)}
        ></button>
      </div>
    </div>
  );
};

export default Carousel;
