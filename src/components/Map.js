import React, { useState, useEffect, memo } from "react";
import { geoCentroid } from "d3-geo";
import {
  ComposableMap,
  Geographies,
  Geography,
  Marker,
  Annotation,
  ZoomableGroup,
} from "react-simple-maps";

import allStates from "../data/allstates.json";

/**
 * offsets object for state labels
 * @typedef {Object} offsets
 * @property {Map} offsets.id - offsets for state labels
 */
const offsets = {
  VT: [50, -8],
  NH: [34, 2],
  MA: [30, -1],
  RI: [28, 2],
  CT: [35, 10],
  NJ: [34, 1],
  DE: [33, 0],
  MD: [47, 10],
  DC: [49, 21],
};

/**
 * Create map component
 * @param {Object} props define map
 * @return {ReactElement} Map component
 */
const Map = (props) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [position, setPosition] = useState({ coordinates: [0, 0], zoom: 1 });
  const locate = props.isState ? `states` : `counties`;

  useEffect(() => {
    async function getData() {
      try {
        const response = await fetch(
          `https://api.covidactnow.org/v2/${locate}.json?apiKey=${process.env.REACT_APP_MAP_API_KEY}`
        );
        if (!response.ok) {
          throw new Error(`this is an error`);
        }
        let actualData = await response.json();
        setData(actualData);
        setError(null);
      } catch (err) {
        setError(err.message);
        setData(null);
      } finally {
        setLoading(false);
      }
    }
    getData();
  }, []);

  function handleMoveEnd(position) {
    setPosition(position);
  }
  console.log(props.isState);
  return (
    <ComposableMap
      projection="geoAlbersUsa"
      data-tip=""
      style={{ width: "100%" }}
      data-testid="map-test"
    >
      <ZoomableGroup
        zoom={position.zoom}
        center={position.coordinates}
        onMoveEnd={handleMoveEnd}
      >
        <Geographies geography={props.mapUrl}>
          {({ geographies }) => (
            <>
              {geographies.map((geo) => {
                const info = data.find((e) => e.fips === geo.id);
                return (
                  info && (
                    <Geography
                      key={geo.rsmKey}
                      stroke="#355070"
                      geography={geo}
                      onMouseEnter={() => {
                        props.setTooltipContent(
                          (props.isState
                            ? "<strong>" + geo.properties.name + "</strong>"
                            : "<strong>" +
                              info.county +
                              ", " +
                              info.state +
                              "</strong>") +
                            "<br>" +
                            "Population: " +
                            info.population.toLocaleString() +
                            "<br>" +
                            "Cases: " +
                            info.actuals.cases.toLocaleString()
                        );
                      }}
                      onMouseLeave={() => {
                        props.setTooltipContent("");
                      }}
                      onClick={() => props.handleClick(geo.properties, info)}
                      style={{
                        default: {
                          fill: "#e56b6f",
                          outline: "none",
                          transition: "all 250ms",
                        },
                        hover: {
                          fill: "#e0c6c6",
                          outline: "none",
                          transition: "all 250ms",
                        },
                        pressed: {
                          fill: "#FF366",
                          outline: "none",
                        },
                      }}
                    />
                  )
                );
              })}

              {props.isState &&
                geographies.map((geo) => {
                  const centroid = geoCentroid(geo);
                  const cur = allStates.find((s) => s.val === geo.id);

                  return (
                    <g key={geo.rsmKey + "-name"}>
                      {cur &&
                        centroid[0] > -160 &&
                        centroid[0] < -67 &&
                        (Object.keys(offsets).indexOf(cur.id) === -1 ? (
                          <Marker coordinates={centroid}>
                            <text y="2" fontSize={14} textAnchor="middle">
                              {cur.id}
                            </text>
                          </Marker>
                        ) : (
                          <Annotation
                            subject={centroid}
                            dx={offsets[cur.id][0]}
                            dy={offsets[cur.id][1]}
                          >
                            <text
                              x={4}
                              fontSize={14}
                              alignmentBaseline="middle"
                            >
                              {cur.id}
                            </text>
                          </Annotation>
                        ))}
                    </g>
                  );
                })}
            </>
          )}
        </Geographies>
      </ZoomableGroup>
    </ComposableMap>
  );
};

export default memo(Map);
