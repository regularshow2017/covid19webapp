import {render, screen, cleanup} from '@testing-library/react';
import References from '../References';
import renderer from 'react-test-renderer';

afterEach(()=>{
    cleanup();
});

test('should render References component', () => {
    render(<References/>);
    const mapStateElement = screen.getByTestId('references-test');
    expect(mapStateElement).toBeInTheDocument();
})

it('renders the References component', () => {
    const tree = renderer.create(<References/>).toJSON();
    expect(tree).toMatchSnapshot();
});