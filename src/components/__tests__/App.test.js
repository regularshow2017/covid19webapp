import React from "react";
import App from "../../App";
import Home from "../../routes/Home";
import aboutApp from "../../routes/aboutApp";
import aboutCovid from "../../routes/aboutCovid";
import Navbar from "../navbar/Navbar";
import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router";
import "@testing-library/jest-dom/extend-expect";
import renderer from "react-test-renderer";

jest.mock("../../routes/Home");
jest.mock("../../components/navbar/Navbar");
jest.mock("../../routes/aboutApp");
jest.mock("../../routes/aboutCovid");

describe("Test for App Router", () => {
  test("should render home page and navbar on default", () => {
    Navbar.mockImplementation(() => <div>NavbarMock</div>);
    Home.mockImplementation(() => <div>HomeMock</div>);
    render(
      <MemoryRouter>
        <App />
      </MemoryRouter>
    );
    expect(screen.getByText("NavbarMock")).toBeInTheDocument();
    expect(screen.getByText("HomeMock")).toBeInTheDocument();
  });

  test("should render page header and aboutApp", () => {
    Navbar.mockImplementation(() => <div>NavbarMock</div>);
    aboutApp.mockImplementation(() => <div>aboutAppMock</div>);
    render(
      <MemoryRouter initialEntries={["/aboutApp"]}>
        <App />
      </MemoryRouter>
    );
    expect(screen.getByText("NavbarMock")).toBeInTheDocument();
    expect(screen.getByText("aboutAppMock")).toBeInTheDocument();
  });

  test("should render page header and aboutCovid", () => {
    Navbar.mockImplementation(() => <div>NavbarMock</div>);
    aboutCovid.mockImplementation(() => <div>aboutCovidMock</div>);
    render(
      <MemoryRouter initialEntries={["/aboutCovid"]}>
        <App />
      </MemoryRouter>
    );
    expect(screen.getByText("NavbarMock")).toBeInTheDocument();
    expect(screen.getByText("aboutCovidMock")).toBeInTheDocument();
  });

  it("renders navbar", () => {
    const tree = renderer.create(<div>NavbarMock</div>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("renders home", () => {
    const tree = renderer.create(<div>HomeMock</div>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("renders aboutApp", () => {
    const tree = renderer.create(<div>aboutAppMock</div>).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
