const express = require("express");
const cors = require("cors");
import { render, screen, cleanup } from "@testing-library/react";
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import firebaseConfig from "../../firebase";

// This section of code tests the back end index.
afterEach(() => {
  cleanup();
});
test("should use express to create an instance of an express server", () => {
  const app = express();
  app.use(cors());
  expect(app).toBeDefined();
});
test("should create and initialize a @firebase/app#FirebaseApp instance and then define app", () => {
  const app = initializeApp(firebaseConfig);
  expect(app).toBeDefined();
});
test("should return an Analytics instance for our application and then define analytics", () => {
  const app = initializeApp(firebaseConfig);
  const analytics = getAnalytics(app);
  expect(analytics).toBeDefined();
});
