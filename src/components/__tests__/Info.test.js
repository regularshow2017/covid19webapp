import {render, screen, cleanup} from '@testing-library/react';
import Info from '../Info';
import coviddata from '../../data/coviddata.json';

afterEach(()=>{
    cleanup();
});

test('should render Info component', () => {
    render(<Info {...coviddata} />);
    const mapStateElement = screen.getByTestId('info-test');
    expect(mapStateElement).toBeInTheDocument();
})