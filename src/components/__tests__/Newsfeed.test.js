import {render, screen, cleanup} from '@testing-library/react';
import Newsfeed from '../Newsfeed';

afterEach(()=>{
    cleanup();
});

test('should render Newsfeed component', () => {
    render(<Newsfeed/>);
    const mapStateElement = screen.getByTestId('newsfeed-test');
    expect(mapStateElement).toBeInTheDocument();
})