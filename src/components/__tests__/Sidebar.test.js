import {render, screen, cleanup} from '@testing-library/react';
import Sidebar from "../header/Sidebar";
import renderer from 'react-test-renderer';

afterEach(()=>{
    cleanup();
});

test('should render sidebar component', () => {
    render(<Sidebar/>);
    const SidebarElement = screen.getByTestId('sidebar-test');
    expect(SidebarElement).toBeInTheDocument();
})

it('renders the Sidebar component', () => {
    const tree = renderer.create(<Sidebar/>).toJSON();
    expect(tree).toMatchSnapshot();
});