import { render, screen, cleanup } from "@testing-library/react";
import Carousel, { CarouselItem } from "../carousel";
import renderer from "react-test-renderer";

afterEach(() => {
  cleanup();
});

test("should render carousel component", () => {
  render(<Carousel />);
  const CarouselElement = screen.getByTestId("carousel-test");
  expect(CarouselElement).toBeInTheDocument();
});
test("should have carousel item", () => {
  expect(CarouselItem).toBeDefined;
});

it("should render carousel item", () => {
  const CarouselItemElement = renderer.create(<CarouselItem />).toJSON();
  expect(CarouselItemElement).toMatchSnapshot();
});
