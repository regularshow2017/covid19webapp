import * as React from "react";
import "./References.css";

/**
 * References component
 * @module
 * @returns {ReactElement}
 */
export default function References() {
  return (
    <div data-testid="references-test" className="references-container">
      <div className="references-title">
        <h1>References and Resources</h1>
      </div>
      <view>
        <p>
          <a
            className="references-link"
            href="https://www.cdc.gov/coronavirus/2019-ncov/index.html"
            target="_blank"
            rel="noreferrer noopener"
          >
            Centers for Disease Control and Prevention (CDC) - Coronavirus
            Disease (COVID-19)
          </a>
        </p>
        <p>
          <a
            className="references-link"
            href="https://en.wikipedia.org/wiki/COVID-19"
            target="_blank"
            rel="noreferrer noopener"
          >
            Wikipedia - Coronavirus Disease (COVID-19)
          </a>
        </p>
        <p>
          <a
            className="references-link"
            href="https://www.who.int/health-topics/coronavirus#tab=tab_1"
            target="_blank"
            rel="noreferrer noopener"
          >
            World Health Organization (WHO) - Coronavirus Disease (COVID-19)
          </a>
        </p>
        <p>
          <a
            className="references-link"
            href="https://www.facebook.com/coronavirus_info/"
            target="_blank"
            rel="noreferrer noopener"
          >
            Facebook - Coronavirus Disease (COVID-19)
          </a>
        </p>
        <p>
          <a
            className="references-link"
            href="https://twitter.com/search?q=%23covid19"
            target="_blank"
            rel="noreferrer noopener"
          >
            Twitter - Coronavirus Disease (COVID-19)
          </a>
        </p>
        <p>
          <a
            className="references-link"
            href="https://www.cdc.gov/mis/index.html"
            target="_blank"
            rel="noreferrer noopener"
          >
            Centers for Disease Control and Prevention (CDC) - Multisystem
            Inflammatory Syndrome (PMIS)
          </a>
        </p>
        <p>
          <a
            className="references-link"
            href="https://en.wikipedia.org/wiki/Multisystem_inflammatory_syndrome_in_children"
            target="_blank"
            rel="noreferrer noopener"
          >
            Wikipedia - Multisystem Inflammatory Syndrome (MIS) in children
          </a>
        </p>
        <p>
          <a
            className="references-link"
            href="https://travel.state.gov/content/travel/en/traveladvisories/COVID-19-Country-Specific-Information.html"
            target="_blank"
            rel="noreferrer noopener"
          >
            Department of State Travel - COVID-19 Country Specific Information
          </a>
        </p>
        <p>
          <a
            className="references-link"
            href="https://travel.state.gov/content/travel/en/traveladvisories/covid-19-travel-information.html"
            target="_blank"
            rel="noreferrer noopener"
          >
            Department of State Travel - COVID-19 Travel Information
          </a>
        </p>
      </view>
    </div>
  );
}
