import React from "react";
import axios from "axios";
import "./Newsfeed.css";

/**
 * Class to create Newsfeed component
 */
class Newsfeed extends React.Component {
  state = {
    articles: [],
    isLoading: true,
    errors: null,
  };

  /**
   * Function to get articles from newsapi.org
   * @return {void}
   */
  getArticles() {
    axios
      .get("http://localhost:8000/news")
      .then((response) =>
        response.data.articles.map((article) => ({
          date: `${article.publishedAt}`,
          title: `${article.title}`,
          url: `${article.url}`,
          urlToImage: `${article.urlToImage}`,
        }))
      )
      .then((articles) => {
        this.setState({
          articles,
          isLoading: false,
        });
      })
      .catch((error) => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.getArticles();
  }

  /**
   * @returns {ReactElement} Newsfeed component
   */
  render() {
    const { isLoading, articles } = this.state;
    return (
      <div data-testid="newsfeed-test" className="newsfeed-container">
        <div className="newsfeed-title">
          <h1>Top US Headlines</h1>
        </div>
        <div className="container">
        {!isLoading ? (
          articles.map((article) => {
            const { date, title, url, urlToImage } = article;
            return (
              <div className="news" key={title}>
                <a className="news-link" href={url}>
                <img className="news-image" src={urlToImage} alt={title} />
                  <p>
                    {title}
                    <p id="date">{date}</p></p>
                    
                  
                </a>
              </div>
            );
          })
        ) : (
          <p>Loading...</p>
        )}</div>
      </div>
    );
  }
}
export default Newsfeed;
